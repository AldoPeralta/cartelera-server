const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
//const useragent = require('express-useragent');
const json2xls = require('json2xls');
const fileUpload = require("express-fileupload");

const AllowAccessProvitional = (req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
};

module.exports = (app) => {
	app.use(AllowAccessProvitional);
	app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));
	app.use(bodyParser.json({ limit: '50mb' }));
	app.use(cookieParser());
	app.use(json2xls.middleware);
	app.use(fileUpload({
		limits: { fileSize: 50 * 1024 * 1024 * 1024 },
	}));
};