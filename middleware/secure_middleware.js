const helmet = require("helmet");

module.exports = (app) => {
	app.set('trust proxy', 1);
	app.use(helmet());
};