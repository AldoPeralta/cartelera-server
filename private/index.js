const GmailSender = require('gmail-send');
const User = require("../db/User");
const Event = require("../db/Event");
const jwt = require("jwt-simple");

const SECRET = "Gk@42em#s$f@NV";

const MiddlewareValidSession = (request, response, nextMiddleware) => {
	try{
		//if(request.hostname === "localhost") response.my_user = { email: "demo" }; return nextMiddleware();
		const my_user = jwt.decode(request.cookies.token, SECRET);
		User.findOne(my_user, (err, user) => {
			if(err){
				return response.status(404).send("");
			}else{
				response.my_user = user;
				return nextMiddleware();
			}
		});
	}catch(e){
		return response.status(404).send("");
	}
};

module.exports = (app) => {
	require("../analytics")(app);

	//Iniciar Sesión
	app.post("/login", async (request, response) => {
		const query = { email: request.body.email, password: request.body.password };
		const user = await User.findOne(query);
		if (!user) return response.send({ error: "user not found" });
		response.cookie('token', jwt.encode(query, SECRET), { maxAge: 60000 * 60 * 12, httpOnly: true });
		return response.send({ success: "OK", level: request.body.email});
	});

	//Cerrar Sesión
	app.get("/logout", async (request, response) => {
		response.cookie('token', "", { maxAge: 0, httpOnly: true });
		return response.redirect("/");
	});

	//Obtener Usuarios
	app.get('/getUsers', MiddlewareValidSession, async (request, response) => {
		const users = await User.find().sort({date:'desc'});
	 	return response.json(users);
	});

	//Agregar Usuario
	app.post('/users/signup', async (request,response) =>{
	    const newUser = new User({...request.body});
	    await newUser.save();
	    return response.send({success:"OK"});
	});

	//Actualizar Usuario
	app.put('/users/edit/:id', async (request,response) =>{
		await User.findByIdAndUpdate(request.params.id,{...request.body});
		return response.send({success:"OK"});
	});

	//Obtener Usuario a Editar
	app.get('/getUser/:id', MiddlewareValidSession, async (request, response) => {
		 const user = await User.findById(request.params.id);
	 	return response.json(user);
	});

	//Eliminar Usuario
	app.get('/users/delete/:id', async (request,resposnse) =>{
		await User.findByIdAndDelete(request.params.id);
		return resposnse.send({success:"OK"});
	});

	//Obtener Eventos por usuario
	app.get('/getEvents', MiddlewareValidSession, async (request, response) => {
	 	const events = await Event.find({email: response.my_user.email}).sort({date:'desc'});
	  	return response.json(events);
	});

	//Obtener todos los Eventos
	app.get('/getAllEvents', async (request, response) => {
		const events = await Event.find().sort({date:'desc'});
		 return response.json(events);
   });

	//Obtener evento a editar
	app.get('/getEvent/:id', MiddlewareValidSession, async (request, response) => {
	  	const evento = await Event.findById(request.params.id);
	  	return response.json(evento);
	});

	//Agregar Evento
	app.post("/events/add", MiddlewareValidSession, async (request, response) => {
		const newEvent = new Event({...request.body, email: response.my_user.email });
		await newEvent.save();
		await request.files.image.mv("./public/img/events/"+newEvent._id+"-image.png");
		await request.files.banner.mv("./public/img/events/"+newEvent._id+"-banner.png");
		return response.send({success:"OK"});
	});

	//Actualizar evento
	app.put("/events/edit/:id", MiddlewareValidSession, async (request, response) => {
		await Event.findOneAndUpdate( request.params.id , {...request.body});
		return response.send({success:"OK"});
	});

	//Actualizar imagen de evento
	app.put("/events/edit-images/:id", MiddlewareValidSession, async (request, response) => {
		const existEvent = await Event.findOne({ _id: request.params.id });
		if (request.files.image) await request.files.image.mv("./public/img/events/"+existEvent._id+"-image.png");
		if (request.files.banner) await request.files.banner.mv("./public/img/events/"+existEvent._id+"-banner.png");
		return response.send({success:"OK"});
	});

	//Eliminar Evento
	app.get('/events/delete/:id', async (request,response) =>{
		await Event.findByIdAndDelete(request.params.id);
		return response.json({success:"OK"});
	});

	app.post('/sendmail', MiddlewareValidSession, (request, response) => {
		GmailSender({
			user:'',
			pass:'',
			to:'',
			subject:'',
			html:''
		})({});
		response.send({success: "ok"});
	});
};