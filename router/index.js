const User = require("../db/User");
const jwt = require("jwt-simple");

const SECRET = "Gk@42em#s$f@NV";

const MiddlewareValidSession = (request, response, nextMiddleware) => {
	try{
		const my_user = jwt.decode(request.cookies.token, SECRET);
		User.findOne(my_user, (err, user) => {
			if(err){
				return response.status(404).send("");
			}else{
				response.my_user = user;
				return nextMiddleware();
			}
		});
	}catch(e){
		return response.redirect("/");
	}
};

const MiddlewareInLogin = (request, response, nextMiddleware) => {
	if(request.cookies.token){
		return response.redirect("/events");
	}

	return nextMiddleware();
};

module.exports = (app) => {
	app.set('view engine', 'ejs');

	app.get("/", MiddlewareInLogin, (req, res, next) => {
		res.render("contents/Login", { data: { title: "Login" } });
	});

	app.get("/events", MiddlewareValidSession, (req, res, next) => {
		res.render("contents/React", { data: { title: "Eventos", name: "cartelera" } });
	});

	app.get("/add-event", MiddlewareValidSession, (req, res, next) => {
		res.render("contents/React", { data: { title: "Nuevo Evento", name: "cartelera" } });
	});

	app.get("/event/edit/:id", MiddlewareValidSession, (req, res, next) => {
		res.render("contents/React", { data: { title: "Editar Evento", name: "cartelera" } });
	});

	app.get("/event/edit-images/:id", MiddlewareValidSession, (req, res, next) => {
		res.render("contents/React", { data: { title: "Editar Imagenes", name: "cartelera" } });
	});

	app.get("/users", MiddlewareValidSession, (req, res, next) => {
		res.render("contents/React", { data: { title: "Usuarios", name: "cartelera" } });
	});

	app.get("/user/edit/:id", MiddlewareValidSession, (req, res, next) => {
		res.render("contents/React", { data: { title: "Editar Usuario", name: "cartelera" } });
	});

	app.get("/signup", MiddlewareValidSession, (req, res, next) => {
		res.render("contents/React", { data: { title: "Nuevo Usuario", name: "cartelera" } });
	});

};