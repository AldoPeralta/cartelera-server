import axios from 'axios';

const BASE_URL = "http://" + window.location.hostname + ":8110";

const GET_EVENTS_ACTION = () => {
    return {
        type: "GET_EVENTS",
        payload: axios.get(BASE_URL + '/getEvents')
    };
}

const GET_EVENT_ACTION = (id) => {
    return {
        type: "GET_EVENT",
        payload: axios.get(BASE_URL + '/getEvent/' + id)
    };
}

const NEW_EVENT_ACTION = (datos) => {
    return {
        type: "NEW_EVENT",
        payload: axios.post(BASE_URL + '/events/add',datos,{
                headers: {
                    'accept': 'application/json',
                    'Content-Type': `multipart/form-data; boundary=${datos._boundary}`
                }
            }
        )
    }
}

const UPDATE_EVENT_ACTION = (id,datos) => {
    return {
        type: "UPDATE_EVENT",
        payload: axios.put(BASE_URL + '/events/edit/' + id, datos,{
                headers: {
                    'accept': 'application/json',
                    'Content-Type': `multipart/form-data; boundary=${datos._boundary}`,
                }
            }
        )
    }
}

const UPDATE_IMAGES_ACTION = (id,datos) => {
    return {
        type: "UPDATE_IMAGES",
        payload: axios.put(BASE_URL + '/events/edit-images/' + id, datos,{
                headers: {
                    'accept': 'application/json',
                    'Content-Type': `multipart/form-data; boundary=${datos._boundary}`,
                }
            }
        )
    }
}

const DELETE_EVENT_ACTION = (id) => {
    return {
        type: "DELETE_EVENT",
        payload: axios.get(BASE_URL + '/events/delete/' + id)
    };
}

export {GET_EVENTS_ACTION,GET_EVENT_ACTION,NEW_EVENT_ACTION,UPDATE_EVENT_ACTION,UPDATE_IMAGES_ACTION,DELETE_EVENT_ACTION};