import React, {Component} from 'react';
import {DISCIPLINES,CATEGORIES, MUNICIPALITIES, PLACES} from '../../data/data';
import {GET_EVENT_ACTION,UPDATE_IMAGES_ACTION} from '../../redux/actions/events/ActionType';
import { connect } from 'react-redux';

class UpdateImages extends Component{

    state = {
        image: null,
        banner: null
    }

    componentWillReceiveProps(nextProps){
        const NewProps = nextProps;

        if(NewProps.responseUpdateImages.success === "OK"){
            window.location.href = "/events";
        }
    }

    componentWillMount(){
        const {id} = this.props.match.params;
        this.props.getEvent(id);
    }

    _getData(){
        let data = new FormData();
        data.append('banner',this.state.banner);
        data.append('image',this.state.image);
        const {id} = this.props.match.params;
        this.props.updateImages(id,data);
    }

    onChangeHandlerBanner = event => {
        this.setState({
            banner: event.target.files[0]
        })
    }

    onChangeHandlerImage = event => {
        this.setState({
            image: event.target.files[0]
        })
    }

    render(){
        return(
            <section className="wow fadeIn">
                <div className="limiter">
                    <div className="container-login100">
                        <div className="wrap-login100">
                            <div className="login100-form-title" style={{backgroundImage:"url(" + require('../../images/culture.jpg') + ")"}}>
                                <span className="login100-form-title-1">Editar Evento</span>
                            </div>

                            <div className="text-center" style={{paddingTop:"15px"}}>
                                <img src={require('../../images/cartelera.png')} className="rounded" alt="Cartelera Logo"/>
                            </div>

                            <form className="login100-form validate-form">

                                <div className="wrap-input100 validate-input m-b-26" style={{paddingTop:"15px",paddingBottom:"15px"}} data-validate="Banner es requerido">
                                    <span className="label-input100">Encabezado</span>
                                    <div className="input-group mb-3">
                                        <div className="custom-file">
                                            <input type="hidden" name="MAX_FILE_SIZE" value="3000000"/>
                                            <input type="file" className="custom-file-input" onChange={this.onChangeHandlerBanner} ref="banner" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01"/>
                                            <label className="custom-file-label" htmlFor="inputGroupFile01">Selecciona un archivo</label>
                                        </div>
                                    </div>
                                </div>

                                <div className="wrap-input100 validate-input m-b-26" style={{paddingTop:"15px",paddingBottom:"15px"}} data-validate="Imagen es requerido">
                                    <span className="label-input100">Imagen Destacada</span>
                                    <div className="input-group mb-3">
                                        <div className="custom-file">
                                            <input type="hidden" name="MAX_FILE_SIZE" value="3000000"/>
                                            <input type="file" className="custom-file-input"onChange={this.onChangeHandlerImage} ref="image" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01"/>
                                            <label className="custom-file-label" htmlFor="inputGroupFile01">Selecciona un archivo</label>
                                        </div>
                                    </div>
                                </div>

                                <div className="container-login100-form-btn">
                                    <input className="login100-form-btn text-center" type="button" onClick={this._getData.bind(this)} defaultValue="Actualizar Datos">

                                    </input>
                                </div>
                            </form> 
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

const mapStateToProps = ({stateEvent,responseUpdateImages}) => {
    return {
        stateEvent: stateEvent,
        responseUpdateImages:responseUpdateImages
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        getEvent: (id) => dispatch(GET_EVENT_ACTION(id)),
        updateImages: (id,datos) => dispatch(UPDATE_IMAGES_ACTION(id,datos))
    };
};

const ConnectUpdateImages =  connect(mapStateToProps,mapDispatchToProps)(UpdateImages);

export default ConnectUpdateImages;