import React, {Component} from 'react';
import {DISCIPLINES,CATEGORIES, MUNICIPALITIES, PLACES} from '../../data/data';
import Schedule from '../../components/Input';
import MainEvent from '../../components/MainEvent';
import {GET_EVENT_ACTION,UPDATE_EVENT_ACTION} from '../../redux/actions/events/ActionType';
import { connect } from 'react-redux';

class UpdateEvent extends Component{

    constructor(props){
        super(props);
        this.state = {
            schedules : [{
                title: "Horario ",
                name: "hour[]"
            }],
            lenght: 1,
            show: false
        }
    }

    componentWillReceiveProps(nextProps){
        const NewProps = nextProps;

        if(NewProps.responseUpdateEvent.success === "OK"){
            window.location.href = "/events";
        }
    }

    componentWillMount(){
        const {id} = this.props.match.params;
        this.props.getEvent(id);
    }

    _getData(){
        let data = new FormData();
        data.append('title',this.refs.title.value);
        data.append('description',this.refs.description.value);
        let discipline = this.refs.discipline.value;
        let color = "";
        DISCIPLINES.map((ds) => {
            if(ds.name === discipline){
                discipline = ds.name;
                color = ds.color;
            }
        })
        data.append('discipline',discipline);
        data.append('color',color);
        data.append('category',this.refs.category.value);
        data.append('type',this.refs.type.value);
        data.append('hierarchy',null);
        data.append('event',this.refs.evento.value);
        data.append('start',this.refs.start.value);
        data.append('finish',this.refs.finish.value);
        data.append('municipality',this.refs.municipality.value);
        let place = this.refs.place.value;
        let address = "";
        PLACES.map((pl) => {
            if(pl.name === place){
                place = pl.name;
                address = pl.address.street + " " + 
                        pl.address.numExt + ", Col. " + 
                        pl.address.colony + " " +
                        pl.address.cp + ", " +
                        pl.address.municipality;

            }
        })
        data.append('place',place);
        data.append('address',address);
        data.append('organizer',this.refs.organizer.value);
        data.append('speaker',this.refs.speaker.value);
        data.append('url',this.refs.url.value);
        data.append('entry',this.refs.entry.value);
        data.append('price',this.refs.price.value);
        data.append('publico',this.refs.public.value);
        data.append('especificPublic',this.refs.especificPublic.value);
        data.append('gender',this.refs.gender.value);

        const {id} = this.props.match.params;
        this.props.updateEvent(id,data);
    }

    render(){
        return(
            <section className="wow fadeIn">
                <div className="limiter">
                    <div className="container-login100">
                        <div className="wrap-login100">
                            <div className="login100-form-title" style={{backgroundImage:"url(" + require('../../images/culture.jpg') + ")"}}>
                                <span className="login100-form-title-1">Editar Evento</span>
                            </div>

                            <div className="text-center" style={{paddingTop:"15px"}}>
                                <img src={require('../../images/cartelera.png')} className="rounded" alt="Cartelera Logo"/>
                            </div>

                            {console.log(this.props.stateEvent)}

                            <form className="login100-form validate-form">
                                
                                <div className="wrap-input100 validate-input m-b-26" style={{paddingTop:"15px",paddingBottom:"15px"}} data-validate="Titulo es requerido">
                                    <span className="label-input100">Titulo del Evento</span>
                                    <input className="input100" type="text" ref="title" defaultValue={this.props.stateEvent.title} required/>
                                    <span className="focus-input100"></span>
                                </div>

                                
                                <div className="wrap-input100 validate-input m-b-26" style={{paddingTop:"15px",paddingBottom:"15px"}} data-validate="Descripcion es requerido">
                                    <span className="label-input100">Descripción</span>
                                    <input ref="description" className="input100" defaultValue={this.props.stateEvent.description} required></input>
                                    <span className="focus-input100"></span>
                                </div>

                                <div className="wrap-input100 validate-input m-b-26" style={{paddingTop:"15px",paddingBottom:"15px"}} data-validate="Disciplina es requerido">
                                    <span className="label-input100">Disciplina</span>
                                    <select className="input100" style={{height:"45px"}} ref="discipline">
                                        <option  value={this.props.stateEvent.discipline}> {this.props.stateEvent.discipline}</option>
                                        {DISCIPLINES.map((discipline,index) => {
                                            return <option value={discipline.name} key={index}>{discipline.name}</option>;
                                        })}
                                    </select>
                                    <span className="focus-input100"></span>
                                </div>

                                <div className="wrap-input100 validate-input m-b-26" style={{paddingTop:"15px",paddingBottom:"15px"}} data-validate="Categoria es requerido">
                                    <span className="label-input100">Categoria</span>
                                    <select className="input100" style={{height:"45px"}} ref="category">
                                        <option value={this.props.stateEvent.category}> {this.props.stateEvent.category}</option>
                                        {CATEGORIES.map((category,index) => {
                                            return <option value={category.name} key={index}>{category.name}</option>;
                                        })}
                                    </select>
                                    <span className="focus-input100"></span>
                                </div>

                                <div className="wrap-input100 validate-input m-b-26" style={{paddingTop:"15px",paddingBottom:"15px"}} data-validate="Tipo de Evento es requerido">
                                    <span className="label-input100">Tipo de Evento</span>
                                    <select className="input100" style={{height:"45px"}} ref="type" required>
                                        <option value={this.props.stateEvent.type}> {this.props.stateEvent.type}</option>
                                        <option value="Eventos">Eventos</option>
                                        <option value="Exposiciones Temporales">Exposiciones Temporales</option>
                                        <option value="Actividades Permanentes">Actividades Permanentes</option>
                                        <option value="Convocatoria">Convocatoria</option>
                                    </select>
                                    <span className="focus-input100"></span>
                                </div>

                                <div className="wrap-input100 validate-input m-b-26" style={{paddingTop:"15px",paddingBottom:"15px"}} data-validate="Evento principal es requerido">
                                    <span className="label-input100">Evento Principal</span>
                                    <input className="input100" defaultValue={this.props.stateEvent.event} type="text" ref="evento"/>
                                    <span className="focus-input100"></span>
                                </div>

                                <div className="wrap-input100 validate-input m-b-26" style={{paddingTop:"15px",paddingBottom:"15px"}} data-validate="Fecha de Inicio es requerido">
                                    <span className="label-input100">Fecha de Inicio</span>
                                    <input className="input100" type="date" ref="start" defaultValue={this.props.stateEvent.start} required/>
                                    <span className="focus-input100"></span>
                                </div>

                                <div className="wrap-input100 validate-input m-b-26" style={{paddingTop:"15px",paddingBottom:"15px"}} data-validate="Fecha de Termino es requerido">
                                    <span className="label-input100">Fecha de Termino</span>
                                    <input className="input100" type="date" ref="finish" defaultValue={this.props.stateEvent.finish} required/>
                                    <span className="focus-input100"></span>
                                </div>

                                {/* <div className="container-login100-form-btn">
                                    <button className="login100-form-btn" onClick={this._add.bind(this)} type="button">
                                        Agregar Horario
                                    </button>
                                </div> */}

                                <div className="wrap-input100 validate-input m-b-26" style={{paddingTop:"15px",paddingBottom:"15px"}} data-validate="Municipio es requerido">
                                    <span className="label-input100">Municipio</span>
                                    <select className="input100" style={{height:"45px"}} ref="municipality" required>
                                        <option value={this.props.stateEvent.municipality}> {this.props.stateEvent.municipality}</option>
                                        {MUNICIPALITIES.map((municipality,index) => {
                                            return <option value={municipality.name} key={index}>{municipality.name}</option>;
                                        })}
                                    </select>
                                    <span className="focus-input100"></span>
                                </div>

                                <div className="wrap-input100 validate-input m-b-26" style={{paddingTop:"15px",paddingBottom:"15px"}} data-validate="Lugar es requerido">
                                    <span className="label-input100">Lugar</span>
                                    <select className="input100" style={{height:"45px"}} ref="place" required>
                                        <option value={this.props.stateEvent.place}>{this.props.stateEvent.place}</option>
                                        {PLACES.map((place,index) => {
                                            return <option value={place.name} key={index}>{place.name}</option>;
                                        })}
                                    </select>
                                    <span className="focus-input100"></span>
                                </div>

                                <div className="wrap-input100 validate-input m-b-26" style={{paddingTop:"15px",paddingBottom:"15px"}} data-validate="Tipo de Evento es requerido">
                                    <span className="label-input100">Organiza</span>
                                    <select className="input100" style={{height:"45px"}} ref="organizer">
                                        <option value={this.props.stateEvent.organizer}>{this.props.stateEvent.organizer}</option>
                                        <option value="Secretaria de Cultura">Secretaria de Cultura</option>
                                        <option value="Organismo Independiente">Organismo Independiente</option>
                                    </select>
                                    <span className="focus-input100"></span>
                                </div>

                                <div className="wrap-input100 m-b-26" style={{paddingTop:"15px",paddingBottom:"15px"}} data-validate="Ponente es requerido">
                                    <span className="label-input100">Imparte</span>
                                    <input className="input100" type="text" ref="speaker" defaultValue={this.props.stateEvent.speaker}/>
                                    <span className="focus-input100"></span>
                                </div>

                                <div className="wrap-input100 m-b-26" style={{paddingTop:"15px",paddingBottom:"15px"}} data-validate="URL es requerido">
                                    <span className="label-input100">URL del evento</span>
                                    <div className="input-group mb-3">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text">www.</span>
                                        </div>
                                        <input type="url" ref="url" defaultValue={this.props.stateEvent.url} className="form-control" aria-label="Dollar amount (with dot and two decimal places)"/>
                                    </div>
                                </div>

                                <div className="wrap-input100 validate-input m-b-26" style={{paddingTop:"15px",paddingBottom:"15px"}} data-validate="Entrada es requerido">
                                    <span className="label-input100">Entrada</span>
                                    <select className="input100" style={{height:"45px"}} ref="entry">
                                        <option value={this.props.stateEvent.entry}>{this.props.stateEvent.entry}</option>
                                        <option value="Gratuito">Gratuito</option>
                                        <option value="Boleto de Cortesia">Boleto de Cortesia</option>
                                        <option value="Cuota de Recuperacion">Cuota de Recuperación</option>
                                        <option value="Inscripcion">Inscripción</option>
                                        <option value="Bono">Bono</option>
                                    </select>
                                    <span className="focus-input100"></span>
                                </div>

                                <div className="wrap-input100 m-b-26" style={{paddingTop:"15px",paddingBottom:"15px"}} data-validate="URL es requerido">
                                    <span className="label-input100">Precio</span>
                                    <div className="input-group mb-3">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text">$</span>
                                        </div>
                                        <input type="number" ref="price" defaultValue={this.props.stateEvent.price} className="form-control" aria-label="Dollar amount (with dot and two decimal places)"/>
                                    </div>
                                </div>

                                {/* <div className="flex-sb-m w-full p-b-30 validate-input" style={{paddingTop:"15px"}} data-validate="Descuentos es requerido">
                                    <span className="label-input100">Descuentos</span>
                                    <div className="contact100-form-checkbox">
                                        <div className="input-group mb-3">
                                            <div className="input-group-prepend">
                                                <div className="input-group-text">
                                                    <input type="checkbox" id="ckb10" ref="discount" aria-label="Checkbox for following text input"/>
                                                </div>
                                            </div>
                                            <input type="text" className="form-control" placeholder="INAPAM" value="INAPAM" aria-label="Text input with checkbox" disabled/>
                                        </div>
                                        <div className="input-group mb-3">
                                            <div className="input-group-prepend">
                                                <div className="input-group-text">
                                                    <input type="checkbox" id="ckb20" ref="discount" aria-label="Checkbox for following text input"/>
                                                </div>
                                            </div>
                                            <input type="text" className="form-control" placeholder="Estudiantes" value="Estudiantes" aria-label="Text input with checkbox"disabled/>
                                        </div>
                                    </div>
                                </div> */}

                                <div className="wrap-input100 validate-input m-b-26" style={{paddingTop:"15px",paddingBottom:"15px"}} data-validate="Publico es requerido">
                                    <span className="label-input100">Público</span>
                                    <select className="input100" style={{height:"45px"}} ref="public">
                                        <option value={this.props.stateEvent.publico}>{this.props.stateEvent.publico}</option>
                                        <option value="General">General</option>
                                        <option value="Niños">Niños</option>
                                        <option value="Adolescentes">Adolescentes</option>
                                        <option value="Jovenes">Jóvenes</option>
                                        <option value="Adultos">Adultos</option>
                                    </select>
                                    <span className="focus-input100"></span>
                                </div>

                                <div className="wrap-input100 validate-input m-b-26" style={{paddingTop:"15px",paddingBottom:"15px"}} data-validate="Publico es requerido">
                                    <span className="label-input100">Público Especifico</span>
                                    <select className="input100" style={{height:"45px"}} ref="especificPublic">
                                        <option value={this.props.stateEvent.especificPublic}>{this.props.stateEvent.especificPublic}</option>
                                        <option value="General">General</option>
                                        <option value="Estudiantes Primaria">Estudiantes Primaria</option>
                                        <option value="Estudiantes Secundaria">Estudiantes Secundaria</option>
                                        <option value="Estudiantes Bachillerato">Estudiantes Bachillerato</option>
                                        <option value="Discapacidad Visual">Discapacidad Visual</option>
                                        <option value="Discapacidad Auditiva">Discapacidad Auditiva</option>
                                        <option value="Discapacidad Motriz">Discapacidad Motriz</option>
                                    </select>
                                    <span className="focus-input100"></span>
                                </div>

                                <div className="wrap-input100 validate-input m-b-26" style={{paddingTop:"15px",paddingBottom:"15px"}} data-validate="Genero es requerido">
                                    <span className="label-input100">Genero</span>
                                    <select className="input100" style={{height:"45px"}} ref="gender">
                                        <option value={this.props.stateEvent.gender}>{this.props.stateEvent.gender}</option>
                                        <option value="General">General</option>
                                        <option value="Hombres">Hombres</option>
                                        <option value="Mujeres">Mujeres</option>
                                    </select>
                                    <span className="focus-input100"></span>
                                </div>

                                {/* <div className="wrap-input100 validate-input m-b-26" style={{paddingTop:"15px",paddingBottom:"15px"}} data-validate="Banner es requerido">
                                    <span className="label-input100">Encabezado</span>
                                    <div className="input-group mb-3">
                                        <div className="custom-file">
                                            <input type="hidden" name="MAX_FILE_SIZE" value="3000000"/>
                                            <input type="file" className="custom-file-input" ref="banner" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01"/>
                                            <label className="custom-file-label" htmlFor="inputGroupFile01">Selecciona un archivo</label>
                                        </div>
                                    </div>
                                </div>

                                <div className="wrap-input100 validate-input m-b-26" style={{paddingTop:"15px",paddingBottom:"15px"}} data-validate="Imagen es requerido">
                                    <span className="label-input100">Imagen Destacada</span>
                                    <div className="input-group mb-3">
                                        <div className="custom-file">
                                            <input type="hidden" name="MAX_FILE_SIZE" value="3000000"/>
                                            <input type="file" className="custom-file-input" ref="image" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01"/>
                                            <label className="custom-file-label" htmlFor="inputGroupFile01">Selecciona un archivo</label>
                                        </div>
                                    </div>
                                </div> */}

                                <div className="container-login100-form-btn">
                                    <input className="login100-form-btn text-center" type="button" onClick={this._getData.bind(this)} defaultValue="Actualizar Datos">

                                    </input>
                                </div>
                            </form> 
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

const mapStateToProps = ({stateEvent,responseUpdateEvent}) => {
    return {
        stateEvent: stateEvent,
        responseUpdateEvent:responseUpdateEvent
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        getEvent: (id) => dispatch(GET_EVENT_ACTION(id)),
        updateEvent: (id,datos) => dispatch(UPDATE_EVENT_ACTION(id,datos))
    };
};

const ConnectUpdateEvent =  connect(mapStateToProps,mapDispatchToProps)(UpdateEvent);

export default ConnectUpdateEvent;