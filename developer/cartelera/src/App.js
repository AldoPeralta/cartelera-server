import React, { Component } from 'react';
import Signup from './views/users/Signup';
import Users from './views/users/Users';
import UpdateUser from './views/users/UpdateUser';
import Events from './views/events/Events';
import AddEvent from './views/events/AddEvent';
import UpdateEvent from './views/events/UpdateEvent';
import UpdateImages from './views/events/UpdateImages';

import { 
	BrowserRouter as Router, 
	Route,
	Switch
} from "react-router-dom";

class App extends Component {
  render() {
    return (
      <Router>
          <Switch>
            <Route path="/signup" component={Signup}/>
            <Route path="/users" component={Users}/>
            <Route path="/user/edit/:id" component={UpdateUser}/>
            <Route path="/events" component={Events}/>
            <Route path="/add-event" component={AddEvent}/>
            <Route path="/event/edit/:id" component={UpdateEvent}/>
            <Route path="/event/edit-images/:id" component={UpdateImages}/>
          </Switch>
      </Router>
    );
  }
}

export default App;
