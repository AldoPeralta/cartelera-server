"use strict";

const port = 8110;
const express = require('express');
const app = express();

require("./middleware")(app);
require("./router")(app);
require("./private")(app);

app.listen(port, () => {
  console.log('Cartelera app listening on port ', port, '!');
});