const mongoose = require('mongoose');

const EventSchema = new mongoose.Schema({
  title: {type: String},
  description: {type: String},
  discipline: {type: String},
  color: {type: String},
  category: {type: String},
  type: {type: String},
  hierarchy: {type: String},
  event: {type: String},
  start: {type: String},
  finish: {type: String},
  dates: {type: Array},
  municipality: {type: String},
  place: {type: String},
  address: {type: String},
  organizer: {type: String},
  speaker: {type: String},
  url: {type: String},
  entry: {type: String},
  price: {type: String},
  discount: {type: String},
  publico: {type: String},
  especificPublic: {type: String},
  gender: {type: String},
  date: {type: Date, default: Date.now()},
  email: String 
});

module.exports = mongoose.model('eventos', EventSchema);